let messages = []

import LogAggregator from './logAggregator'

let globalAccountKey: string = ''
let globalGraphName: string = ''

class LlamaLogs {
	static init(options) {
		const {accountKey = '', graphName = ''} = options
		globalAccountKey = accountKey
		globalGraphName = graphName
	}

	static pointStat(params) {
		LlamaLogs.stat({...params, type: 'point'})
	}

	static avgStat(params) {
		LlamaLogs.stat({...params, type: 'average'})
	}

	static maxStat(params) {
		LlamaLogs.stat({...params, type: 'max'})
	}

	static stat(params) {
		const {component, type, name, value, graphName, accountKey} = params
		if (!component || !type || !name || !value) return
		
		const startTimestamp = Date.now()
		
		const message = {
				component,
				timestamp: startTimestamp,
				name,
				value,
				type,
				account: accountKey || globalAccountKey,
				graph: graphName || globalGraphName
			}

		LogAggregator.addStat(message)
	}
	// could return response to be logged

	// could link the call/response objects by giving random id and then
	// passing into the return event. Save call as error log related.
	// give all dots id's??? Find matching ones???

	// set tracking id on outer call, keep static ref
	// log on all inner calls until the clear/return function is called.
	static log(params) {
		const startTimestamp = Date.now()
		const {sender, receiver, log, graphName, accountKey} = params
		if (!sender || !receiver) return
		if (!(graphName || globalGraphName)) return

		const message = {
				sender,
				receiver,
				timestamp: startTimestamp,
				log: log || '',
				initialMessage: true,
				account: accountKey || globalAccountKey || -1,
				graph: graphName || globalGraphName
			}

		LogAggregator.addMessage(message)

		const returnEvent = (innerParams:any  = {}) => {
			const {error = false, log = ''} = innerParams
			const endTimestamp = Date.now()

			const message = {
				sender: receiver,
				receiver: sender,
				timestamp: endTimestamp,
				elapsed: endTimestamp - startTimestamp,
				error,
				log,
				initialMessage: false,
				account: globalAccountKey || -1,
				graph: graphName || globalGraphName
			}

			LogAggregator.addMessage(message)
		}

		return returnEvent
	}

	static forceSend() {
		LogAggregator.sendMessages()
	}

	static messages() {
		return messages
	}
}

export {LlamaLogs, LlamaLogs as LLL}