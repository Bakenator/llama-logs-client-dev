const request = require('request');

const isDev = true
const url = isDev ? 'http://localhost:4000' : 'https://llamalogs.com'

export default class LlamaProxy {
    static sendMessages(aggregateLogs, aggregateStats) {
		const timestamp = Date.now()

		// turning into array of aggregates
		const messageArr = []
		Object.keys(aggregateLogs).forEach(sender => {
			Object.keys(aggregateLogs[sender]).forEach(receiver => {
				messageArr.push({
					sender, 
					receiver,
					count: aggregateLogs[sender][receiver].total,
					errorCount: aggregateLogs[sender][receiver].errors,
					log: aggregateLogs[sender][receiver].log,
					errorLog: aggregateLogs[sender][receiver].errorLog,
					clientTimestamp: Date.now(),
					graph: aggregateLogs[sender][receiver].graph || 'noGraph',
					account: aggregateLogs[sender][receiver].account,
					initialMessageCount: aggregateLogs[sender][receiver].initialMessageCount
				})
			})
		})

		if(isDev) {
			console.log('messageArr')
			console.log(messageArr)
		}	

		const postData = {
			time_logs: messageArr
		}
		
		const reqObject = {
			
			url: `${url}/api/timelogs`, 
			method: 'post',
	        body: JSON.stringify(postData),
	        headers: {
	            'Content-Type': 'application/json'
	        }
		}
		if (messageArr.length) {
			request(reqObject, (error, response, body) => {
				// console.log('timelog error')
				// console.log(error)
				// console.log('timelog body')
				// console.log(body)
			});

			// console.log('aggregateStats')
			// console.log(aggregateStats)
		}

		const statList = []
		// { Delegator:
  //  { queueLength:
  //     { component: 'Delegator',
  //       timestamp: 1573352659529,
  //       name: 'queueLength',
  //       value: 416,
  //       type: 'point' } } }

		// component level
		Object.keys(aggregateStats).forEach(component => {
			Object.keys(aggregateStats[component]).forEach(name => {
				const ob = aggregateStats[component][name]
				// ob.graph = 'firstGraph'
				ob.clientTimestamp = ob.timestamp
				statList.push(ob)
			})
		})

		if(isDev) {
			console.log('statlist')
			console.log(statList)
		}


		const statPostData = {
			time_stats: statList
		}

		const statReqObject = {
			url: `${url}/api/timestats`, 
			method: 'post',
	        body: JSON.stringify(statPostData),
	        headers: {
	            'Content-Type': 'application/json'
	        }
		}

		if (statList.length) {
			request(statReqObject, (error, response, body) => {
				// console.log(body)
			});
		}
	}
}